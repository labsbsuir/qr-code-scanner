﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

public class QRCodeGenerator : MonoBehaviour
{
    [SerializeField] private Image qrImage;
    [SerializeField] private InputField input;

    private static Color32[] Encode(string textForEncoding, int width, int height)
    {
        QRCodeWriter qrEncode = new QRCodeWriter();

        Dictionary<EncodeHintType, object> hints = new Dictionary<EncodeHintType, object>
        {
            {EncodeHintType.CHARACTER_SET, "utf-8"}
        };

        BitMatrix qrMatrix = qrEncode.encode(
            textForEncoding,
            BarcodeFormat.QR_CODE,
            width,
            height,
            hints);

        return new BarcodeWriter().Write(qrMatrix);
    }

    public void ReturnToMainMenu()
    {
        UIManager.I.OpenMainMenu();
    }

    public Texture2D GenerateQRCode(string text)
    {
        var encoded = new Texture2D(512, 512);
        var color32 = Encode(text, encoded.width, encoded.height);
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }

    public void Generate()
    {
        string text = input.text;
        var texture = GenerateQRCode(text);
        qrImage.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), 0.5f * Vector2.one);
    }
}
