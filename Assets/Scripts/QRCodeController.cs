﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QRCodeController : MonoBehaviour
{
    [SerializeField] private QRScanner qRScanner;
    [SerializeField] private Text decodedTextView;
    [SerializeField] private Button openUrlButton;
    [SerializeField] private Button scanAgainButton;
    [SerializeField] private GameObject background;

    private string decodedText;

    private UniWebView webView;
    public void OnEnable()
    {
        Subscribe();
    }

    private void OnWindowOpen(WindowType obj)
    {
        if (obj == WindowType.ScanMenu)
        {
            background.gameObject.SetActive(false);
            scanAgainButton.gameObject.SetActive(false);
            openUrlButton.gameObject.SetActive(false);
            qRScanner.ActivateScanner();
        }
    }

    private void OnQrCodeScan(string s)
    {
        if (!string.IsNullOrEmpty(s))
        {
            bool result = Uri.TryCreate(s, UriKind.Absolute, out var uriResult)
                          && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            background.SetActive(true);
            if (result)
            {
                openUrlButton.gameObject.SetActive(true);
            }

            scanAgainButton.gameObject.SetActive(true);
            decodedText = s;
            decodedTextView.text = decodedText;
        }
    }

    private void OpenUrl()
    {
        OpenUrl(decodedText);
    }

    private void ScanAgain()
    {
        background.SetActive(false);
        scanAgainButton.gameObject.SetActive(false);
        openUrlButton.gameObject.SetActive(false);
        qRScanner.ActivateScanner();
    }

    private void Unsubscribe()
    {
        qRScanner.OnQrCodeScan -= OnQrCodeScan;
        scanAgainButton.onClick.RemoveListener(ScanAgain);
        openUrlButton.onClick.RemoveListener(OpenUrl);
        UIManager.OnWindowOpen -= OnWindowOpen;
    }

    private void Subscribe()
    {
        qRScanner.OnQrCodeScan += OnQrCodeScan;
        openUrlButton.onClick.AddListener(OpenUrl);
        scanAgainButton.onClick.AddListener(ScanAgain);
        UIManager.OnWindowOpen += OnWindowOpen;
    }

    public void OpenUrl(string url)
    {
        webView = gameObject.AddComponent<UniWebView>();
        webView.OnShouldClose += view =>
        {
            Destroy(webView);
            webView = null;
            return true;
        };

        webView.Frame = new Rect(0, 0, Screen.width, Screen.height);
        webView.Load(url);
        webView.Show(true, UniWebViewTransitionEdge.Bottom);
    }

    private void OnDisable()
    {
        Unsubscribe();
    }

    public void ReturnToMainMenu()
    {
        qRScanner.StopDecode();
        UIManager.I.OpenMainMenu();
    }

}
