﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager I;
    private UIWindow currentWindow;
    [SerializeField] private List<UIWindow> windows = new List<UIWindow>();
    public static event Action<WindowType> OnWindowOpen;
    private void Awake()
    {
        if (I == null)
        {
            I = this;
        }

        HideAllWindows();
        OpenMainMenu();
    }

    private UIWindow FindWindow(WindowType type)
    {
        return windows.FirstOrDefault(x => x.type == type);
    }

    private UIWindow OpenWindow(WindowType type)
    {
        currentWindow?.Hide();
        var window = FindWindow(type).Open();
        OnWindowOpen?.Invoke(type);
        return window;
    }

    private void HideAllWindows()
    {
        foreach (var window in windows)
        {
            window.Hide();
        }
    }

    public void OpenMainMenu()
    {
        currentWindow = OpenWindow(WindowType.MainMenu);
    }

    public void OpenGenerateMenu()
    {
        currentWindow = OpenWindow(WindowType.GenerateMenu);

    }

    public void OpenScanMenu()
    {
        currentWindow = OpenWindow(WindowType.ScanMenu);
    }
}

[Serializable]
public class UIWindow
{
    public List<GameObject> objects = new List<GameObject>();
    public WindowType type;

    public UIWindow Open()
    {
        foreach (var obj in objects)
        {
            obj.SetActive(true);
        }
        return this;
    }

    public void Hide()
    {
        foreach (var obj in objects)
        {
            obj.SetActive(false);
        }
    }

}

public enum WindowType
{
    MainMenu,
    GenerateMenu,
    ScanMenu
}