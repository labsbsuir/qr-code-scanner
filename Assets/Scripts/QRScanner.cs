﻿using UnityEngine;
using UnityEngine.UI;
using System;
using ZXing;
using System.Threading;

public class QRScanner : MonoBehaviour
{
    [SerializeField] private RawImage cameraView;
    private WebCamTexture webCamTexture;
    public event Action<string> OnQrCodeScan;
    private Color32[] pixels;
    private Thread qrThread;
    private int width;
    private int height;
    private string decodedText;
    private bool canDecode;

    private void Init()
    {
        ActivateScanner();
    }

    private void PrepareThread()
    {
        qrThread?.Abort();
        qrThread = new Thread(DecodeQR);
        qrThread.Start();
    }

    public void ActivateScanner()
    {
        InitScanner();
        PrepareThread();
        TakePictureParameters();
        decodedText = string.Empty;
        canDecode = true;
    }

    public void InitScanner()
    {
        webCamTexture = new WebCamTexture { requestedHeight = Screen.height, requestedWidth = Screen.width };
        cameraView.texture = webCamTexture;
        webCamTexture.Play();
    }

    private void TakePictureParameters()
    {
        width = webCamTexture.width;
        height = webCamTexture.height;
    }

    private void DecodeQR()
    {
        var barcodeReader = new BarcodeReader { AutoRotate = true, TryInverted = true };
        while (true)
        {
            try
            {
                var result = barcodeReader.Decode(pixels, width, height);
                if (result != null)
                {
                    decodedText = result.Text;
                    Debug.Log("---------RESULT---------- " + result.Text);
                    pixels = null;
                    break;
                }

                Thread.Sleep(50);
                pixels = null;
            }
            catch
            {

            }
        }
    }

    private void Update()
    {
        if (canDecode)
        {
            GetPixels();
            CheckDecodedText();
        }
    }

    private void GetPixels()
    {
        if (pixels == null)
        {
            pixels = webCamTexture.GetPixels32();
        }
    }

    private void CheckDecodedText()
    {
        if (!string.IsNullOrEmpty(decodedText))
        {
            OnQrCodeDecode(decodedText);
        }
    }

    private void OnQrCodeDecode(string s)
    {
        OnQrCodeScan?.Invoke(s);
        StopDecode();
    }

    public void StopDecode()
    {
        canDecode = false;
        qrThread?.Abort();
        webCamTexture?.Stop();
    }

}
